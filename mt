namespace MT
{

    class Pair   // это левая часть одного правила МТ , будет ключом для словаря.
    {
        public char a; // обозреваемая буква
        public int p;  // состояние МТ
        public Pair(char aa, int pp)
        {
            a = aa; p = pp;
        }
    }
    class Triplet // это правая часть одного правила МТ, будет значением, соответствующим ключу.
    {
        public char b; // новая буква
        public int q; // новое состояние МТ
        public char mov; // символ движения L,R или N (на месте)
        public Triplet(char bb, int qq, char m)
        {
            b = bb; q = qq; mov = m;
        }
    }
    Class Rule
    {
        Тут будут состояния
        0) если a, то a, состояние не изменится
           если б, то б -> c, состояние 0 переходит в состояние 1
        1) если , то с -> д, состояние 1 переходит в состояние 2
        2) если д, то д, состояние не изменится
           если е, то е -> с, состояние 2 переходит в состояние 1
           и тд
           
        
         
         
         Мы знаем, как двигаться влево и вправо. ура. ещё нужно стоять на месте
         
         
         
         
         В итоге наши правила выглядят так:
         есть слово abAa
         
         1) a0,a0R (ничего не поменялось, двигаемся вправо)
         2) b0,c1N (b -> c, никуда не пошли, теперь у нас это состояние 1)
         3) с1,d2R (с -> d, пошли вправо, и у этой d состояние 2)
         
         в итоге у нас:
         1)abAa
         2)acAa
         3)adAa, и тут мы стоим на букве а. Что дальше делать с A (я её выделила специально)
         
         То есть по всему слову через for, наверное, двигаться не нужно. У нас каждый шаг отдельно. 
         А ещёёёёёёёёёёёёё
         Я поняла, что не нужно расписывать состояния (типа как правила). Нам их просто нужно запоминать. 
         В данном случае это типа как индексы, но не индексы. И эти чиселки надо запоминать. А запоминать их будет  int stateMT, который изначально =0.
         
         
         И ещё. Я уПОРНО не хочу читать слово из блокнота. Поэтому давай оставим его во втором текстбоксе, котоый переносит его в LB.
         
         
         
    }
    
    class Form1 : Form
    {
        // Словарь для хранения программы МТ:
        public Dictionary<Pair, Triplet> programMT = new Dictionary<Pair, Triplet>();
        int stateMT = 0;  // текущее состояние МТ
        int state = 0;    // текущее положение головки МТ
        string word;  // текушее слово (у вас в этой роли input)
        
        // описание видимого содержимого формы 
        Label lb = new Label();
        TextBox t = new TextBox();
        TextBox r = new TextBox();
        Button b0 = new Button();
        Button b2 = new Button();

       

        public Form1()
        {
            lb.Location = new Point(10, 10);
            lb.Size = new Size(300, 50);
            lb.BackColor = Color.White;
            t.Location = new Point(10, lb.Top + lb.Height + 5);
            t.Size = new Size(300, 20);
            r.Location = new Point(10, t.Top + t.Height + 5);
            r.Size = new Size(300, 20);
            b0.Location = new Point(320, 63);
            b0.Text = "Word";
            b0.Click += new EventHandler(b0_Click);
            b0.BackColor = Color.Red;
            b0.ForeColor = Color.Silver;


            b2.Location = new Point(320, 89);
            b2.Text = "Rule";
            b2.Click += new EventHandler(b2_Click);
            b2.BackColor = Color.Red;
            b2.ForeColor = Color.Silver;

            this.Size = new Size(450, 200);
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.Sizable;
            this.BackColor = Color.Black;
            this.AcceptButton = b0;
            this.Controls.Add(lb);
            this.Controls.Add(t);
            this.Controls.Add(r);
            this.Controls.Add(b0);
            this.Controls.Add(b2);
            this.ActiveControl = t;

//Тут я читаю файл из блокнота

            t.Focus();
            t.SelectionStart = state;
            t.SelectionLength = 1;
        }

        void b0_Click(object sender, EventArgs e)
        {
            lb.Text = t.Text;
            t.Clear();
        }

        void b2_Click(object sender, EventArgs e)
        {
           // lb.Text = 

           // return;
        }
    }



    class Program
    {
        [STAThread]
        static void Main()
        {
            Form1 f = new Form1();
            Application.Run(f);
        }
    }
}